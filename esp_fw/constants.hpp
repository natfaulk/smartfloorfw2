#ifndef CONSTANTS_HPP
#define CONSTANTS_HPP

#include <cstdint>

#include "common.hpp"

const char*     VERSION_STRING                          = "1.0.2";
const char*     BOARD_ID                                = "OLD1";

const uint32_t  BAUD_RATE                               = 115200;
const char*     SSID                                    = "VlpNetwork";
const char*     PASSWORD                                = "hiddenpassword";
const char*     HOST                                    = "192.168.88.10";
const int       PORT                                    = 3000;

#endif
