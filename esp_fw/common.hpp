#ifndef COMMON_HPP
#define COMMON_HPP

#include <cstdint>

// Variables used in both the ESP and M3 firmwares

const char*     ID_SUBS_STRING                          = "{\"ID\":\"__ID\"";
const uint8_t   ID_OFFSET                               = 7;

#endif
