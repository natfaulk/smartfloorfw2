#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <ESP8266HTTPClient.h>
#include <cstring>

#include "debug.hpp"
#include "constants.hpp"

const uint32_t IN_BUFF_LEN = 5000;
char inBuff[IN_BUFF_LEN] = {0};
uint32_t i_inBuff = 0;

bool sendData(char* data);
void substituteId(char* data);

HTTPClient http;

void setup(void)
{
  DBG_Setup(BAUD_RATE);
  DBG_nl();
  DBG_s("Setup started");
  DBG_pair("Board ID", BOARD_ID);
  DBG_pair("FW Version", VERSION_STRING);

  char hostname[20];
  snprintf(hostname, 20, "sf_%d", BOARD_ID);
  WiFi.hostname(hostname);
  DBG_s("Hostname set to ");DBG_s(hostname);

  WiFi.mode(WIFI_STA);
  WiFi.begin(SSID, PASSWORD);
  DBG_s("Connecting to ", false);DBG_s(SSID);
  
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    DBG_s(".", false);
  }
  DBG_nl();

  DBG_s("Connection successful");  
  DBG_pair("IP Address", WiFi.localIP().toString().c_str());

  delay(1000);

  DBG_s("Setup finished");
}

void loop(void)
{
  if (Serial.available() > 0)
  {
    char tempIn = Serial.read();

    if (tempIn == '\n')
    {
      inBuff[i_inBuff++] = '\0';

      if(sendData(inBuff)) DBG_s("Sent data success");
      else DBG_s("Sent data failed");

      i_inBuff = 0;
    }
    else inBuff[i_inBuff++] = tempIn;

    if (i_inBuff >= IN_BUFF_LEN - 1) i_inBuff = 0;
  }
}

bool sendData(char* data)
{
  substituteId(data);

  bool returnVal = false;
  WiFiClient client;
  http.begin(client, HOST, PORT, "/submit");
  http.addHeader("Content-Type", "application/json");
      
  int httpCode = http.POST(data);
  if(httpCode > 0) {
    if(httpCode == HTTP_CODE_OK) {
      String payload = http.getString();
      if (payload == "ack") returnVal = true;
    }
  }
  http.end();
  return returnVal;
}

void substituteId(char* data)
{
  // as strings are not same length use strncmp to stop comparing with the terminating \0
  if(strncmp(ID_SUBS_STRING, data, strlen(ID_SUBS_STRING)) == 0)
  {
    // use memcpy instead of strcpy else the \0 gets copied
    // and terminates the string early
    memcpy(data + ID_OFFSET, BOARD_ID, strlen(BOARD_ID));
  }
}
