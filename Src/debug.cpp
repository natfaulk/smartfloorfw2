#include "debug.hpp"

static bool _nl = true;

void DBG_Setup(UART_HandleTypeDef *_huart)
{
#if DEBUG_ENABLE
  UART_Init(_huart);
#endif
}

void DBG_s(const char *s, bool newline)
{
#if DEBUG_ENABLE  
  if (_nl) UART_SendStr("[INFO] ");
  UART_SendStr(s);
  if (newline) DBG_nl();
  else _nl = false;
#endif
}

void DBG_f(double n, bool newline)
{
#if DEBUG_ENABLE  
  if (_nl) UART_SendStr("[INFO] ");
  UART_SendDouble(n);
  if (newline) DBG_nl();
  else _nl = false;
#endif
}

void DBG_i(uint32_t n, bool newline)
{
#if DEBUG_ENABLE  
  if (_nl) UART_SendStr("[INFO] ");
  UART_SendUint32(n);
  if (newline) DBG_nl();
  else _nl = false;
#endif
}

void DBG_nl(void)
{
#if DEBUG_ENABLE
  _nl = true;
  UART_SendStr("\r\n");
#endif
}

void DBG_pair(const char *s, double n)
{
#if DEBUG_ENABLE
  if (_nl) UART_SendStr("[INFO] ");
  UART_SendStr(s);
  UART_SendStr(": ");
  UART_SendDouble(n);
  _nl = true;
#endif
}

void DBG_pair(const char *s, uint32_t n)
{
#if DEBUG_ENABLE
  if (_nl) UART_SendStr("[INFO] ");
  UART_SendStr(s);
  UART_SendStr(": ");
  UART_SendUint32(n);
  _nl = true;
#endif
}

void DBG_pair(const char *s, const char* s2)
{
#if DEBUG_ENABLE
  if (_nl) UART_SendStr("[INFO] ");
  UART_SendStr(s);
  UART_SendStr(": ");
  UART_SendStr(s2);
  _nl = true;
#endif
}
