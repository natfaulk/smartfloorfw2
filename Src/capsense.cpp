#include "CapSense.hpp"
#include "n_timer.h"

const int32_t TIMEOUT = 20000;

CapSense::CapSense(GPIO_TypeDef* _sport, uint32_t _spin, GPIO_TypeDef* _rport, uint32_t _rpin, int32_t _id):
sport(_sport), spin(_spin), rport(_rport), rpin(_rpin), id(_id)
{
}

int32_t CapSense::read(uint32_t nSamples)
{
  int32_t count = 0;

  for (uint32_t i = 0; i < nSamples; i++)
  {
    // make sure input pin charged to 0
    // first in needs to be an out
    this->sport->ODR &= ~(0x1 << this->spin); // spin low
    if (this->rpin < 8)
    {
      this->rport->CRL &= ~(0x0000000F << (this->rpin * 4)); // rpin as out
      this->rport->CRL |= (0x2 << (this->rpin * 4)); // rpin as out
    } else {
      this->rport->CRH &= ~(0x0000000F << ((this->rpin - 8) * 4)); // rpin as out
      this->rport->CRH |= (0x2 << ((this->rpin - 8) * 4)); // rpin as out
    }
    this->rport->ODR &= ~(0x1 << this->rpin); // rpin low
    NTIM_Start();
    while (NTIM_Micros() < 10);
    NTIM_Stop();
    if (this->rpin < 8)
    {
      this->rport->CRL &= ~(0x0000000F << (this->rpin * 4)); //rpin back as input
      this->rport->CRL |= (0x4 << (this->rpin * 4));
    } else {
      this->rport->CRH &= ~(0x0000000F << ((this->rpin - 8) * 4)); //rpin back as input
      this->rport->CRH |= (0x4 << ((this->rpin - 8) * 4));
    }
    //start
    this->sport->ODR |= (0x1 << this->spin); // spin high

    while (!(this->rport->IDR & (1 << this->rpin)) && count < TIMEOUT) count++;
  }

  return count;
}
