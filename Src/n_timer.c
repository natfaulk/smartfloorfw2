#include "n_timer.h"

static volatile TIM_HandleTypeDef *htim;
static volatile uint32_t micros = 0;

void NTIM_Init(TIM_HandleTypeDef *_htim)
{
  htim = _htim;
}

void NTIM_Start(void)
{
  micros = 0;
  __HAL_TIM_ENABLE_IT(htim, TIM_IT_UPDATE);
  __HAL_TIM_ENABLE(htim);
}

uint32_t NTIM_Stop(void)
{
  __HAL_TIM_DISABLE_IT(htim, TIM_IT_UPDATE);
  __HAL_TIM_DISABLE(htim);
  return micros;
}

uint32_t NTIM_Micros(void)
{
  return micros;
}

void TIM2_IRQHandler(void)
{
  __HAL_TIM_CLEAR_IT(htim, TIM_IT_UPDATE);
  micros++;
}
