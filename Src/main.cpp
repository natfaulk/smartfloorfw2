/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "debug.hpp"
#include "n_timer.h"
#include "capSense.hpp"
#include "n_uart3.h"
#include "../esp_fw/common.hpp"
#include <array>
#include <memory>
#include <string>

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_TIM2_Init();
  MX_USART1_UART_Init();
  MX_USART3_UART_Init();
  /* USER CODE BEGIN 2 */

  DBG_Setup(&huart1);
  DBG_s("Setup started");
  HAL_Delay(100);

  UART3_Init(&huart3);
  HAL_Delay(100);
  NTIM_Init(&htim2);

  HAL_Delay(1000);

  // CapSense c1(GPIOD, 1, GPIOD, 2);

  std::array<std::shared_ptr<CapSense>, 36> sensors;

  uint32_t s_i = 0;
  sensors.at(s_i++) = std::make_shared<CapSense>(GPIOD, 14, GPIOD, 15,  1);       // 1
  sensors.at(s_i++) = std::make_shared<CapSense>(GPIOA,  2, GPIOA,  3,  2);       // 2
  sensors.at(s_i++) = std::make_shared<CapSense>(GPIOA,  4, GPIOA,  5,  3);       // 3
  sensors.at(s_i++) = std::make_shared<CapSense>(GPIOA,  6, GPIOA,  7,  4);       // 4

  sensors.at(s_i++) = std::make_shared<CapSense>(GPIOA,  8, GPIOA, 11,  5);       // 5
  sensors.at(s_i++) = std::make_shared<CapSense>(GPIOA, 12, GPIOA, 13,  6);       // 6
  sensors.at(s_i++) = std::make_shared<CapSense>(GPIOA, 14, GPIOA, 15,  7);       // 7
  sensors.at(s_i++) = std::make_shared<CapSense>(GPIOC,  0, GPIOC,  1,  8);       // 8

  sensors.at(s_i++) = std::make_shared<CapSense>(GPIOC,  2, GPIOC,  3,  9);       // 9
  sensors.at(s_i++) = std::make_shared<CapSense>(GPIOC,  4, GPIOC,  5, 10);       // 10
  sensors.at(s_i++) = std::make_shared<CapSense>(GPIOC,  6, GPIOC,  7, 11);       // 11
  sensors.at(s_i++) = std::make_shared<CapSense>(GPIOC,  8, GPIOC,  9, 12);       // 12

  sensors.at(s_i++) = std::make_shared<CapSense>(GPIOC, 10, GPIOC, 11, 13);       // 13
  sensors.at(s_i++) = std::make_shared<CapSense>(GPIOE,  4, GPIOE,  5, 14);       // 14
  sensors.at(s_i++) = std::make_shared<CapSense>(GPIOC, 14, GPIOC, 15, 15);       // 15
  sensors.at(s_i++) = std::make_shared<CapSense>(GPIOB,  8, GPIOB,  9, 16);       // 16

  sensors.at(s_i++) = std::make_shared<CapSense>(GPIOE,  0, GPIOE,  1, 17);       // 17
  sensors.at(s_i++) = std::make_shared<CapSense>(GPIOE,  2, GPIOE,  3, 18);       // 18
  sensors.at(s_i++) = std::make_shared<CapSense>(GPIOE,  6, GPIOC, 13, 19);       // 19
  sensors.at(s_i++) = std::make_shared<CapSense>(GPIOE,  8, GPIOE,  9, 20);       // 20
  
  sensors.at(s_i++) = std::make_shared<CapSense>(GPIOE, 10, GPIOE, 11, 21);       // 21
  sensors.at(s_i++) = std::make_shared<CapSense>(GPIOE, 12, GPIOE, 13, 22);       // 22
  sensors.at(s_i++) = std::make_shared<CapSense>(GPIOE, 14, GPIOE, 15, 23);       // 23
  sensors.at(s_i++) = std::make_shared<CapSense>(GPIOB,  0, GPIOB,  1, 24);       // 24

  sensors.at(s_i++) = std::make_shared<CapSense>(GPIOD,  7, GPIOB,  3, 25);       // 25
  sensors.at(s_i++) = std::make_shared<CapSense>(GPIOB,  4, GPIOB,  5, 26);       // 26
  sensors.at(s_i++) = std::make_shared<CapSense>(GPIOB,  6, GPIOB,  7, 27);       // 27
  sensors.at(s_i++) = std::make_shared<CapSense>(GPIOB, 12, GPIOB, 13, 28);       // 28

  sensors.at(s_i++) = std::make_shared<CapSense>(GPIOB, 14, GPIOB, 15, 29);       // 29
  sensors.at(s_i++) = std::make_shared<CapSense>(GPIOC, 12, GPIOD,  0, 30);       // 30
  sensors.at(s_i++) = std::make_shared<CapSense>(GPIOD,  1, GPIOD,  2, 31);       // 31
  sensors.at(s_i++) = std::make_shared<CapSense>(GPIOD,  3, GPIOD,  4, 32);       // 32

  sensors.at(s_i++) = std::make_shared<CapSense>(GPIOD,  5, GPIOD,  6, 33);       // 33
  sensors.at(s_i++) = std::make_shared<CapSense>(GPIOD,  8, GPIOD,  9, 34);       // 34
  sensors.at(s_i++) = std::make_shared<CapSense>(GPIOD, 10, GPIOD, 11, 35);       // 35
  sensors.at(s_i++) = std::make_shared<CapSense>(GPIOD, 12, GPIOD, 13, 36);       // 36


  DBG_s("Setup finished");

//  DBG_i(GPIOA->CRH);
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */
    /* USER CODE BEGIN 3 */

    std::string out;
    out.reserve(2000);
    // id is replaced on the esp. Using a 4 digit number for now
    out = ID_SUBS_STRING;
    for (auto i:sensors)
    {
      // DBG_pair(std::to_string(i->id).c_str(), uint32_t(i->read(50)));
      out += ",\"p";
      out += std::to_string(i->id);
      out += "\":";
      out += std::to_string(i->read(50));
    }
    out += "}\n";
    UART3_SendStr(out.c_str());

    HAL_Delay(100);
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Initializes the CPU, AHB and APB busses clocks
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
