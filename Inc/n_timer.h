#ifndef N_TIMER_H
#define N_TIMER_H

#ifdef __cplusplus
extern "C" {
#endif

#include "stm32f1xx_hal.h"

void NTIM_Init(TIM_HandleTypeDef *_htim);
uint32_t NTIM_Stop(void);
void NTIM_Start(void);
uint32_t NTIM_Micros(void);

#ifdef __cplusplus
}
#endif

#endif /* end of include guard: UART_H */
