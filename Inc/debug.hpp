#ifndef DEBUG_HPP
#define DEBUG_HPP

#include <cstdint>

#include "n_uart.h"
#include "stm32f1xx_hal.h"

#define DEBUG_ENABLE 1

void DBG_Setup(UART_HandleTypeDef *_huart);
void DBG_s(const char *s, bool = true);
void DBG_f(double n, bool = true);
void DBG_i(uint32_t n, bool = true);
void DBG_nl(void);

void DBG_pair(const char *s, double n);
void DBG_pair(const char *s, uint32_t n);
void DBG_pair(const char *s, const char* s2);


#endif
