#ifndef UART3_H
#define UART3_H

#ifdef __cplusplus
extern "C" {
#endif

#include "stm32f1xx_hal.h"

void UART3_Init(UART_HandleTypeDef *_huart);
void UART3_SendStr(const char * str);
void UART3_SendUint32(uint32_t i);
void UART3_SendDouble(double f);

void UART3_PutChar(char c);
uint16_t UART3_CharsAvailable(void);
uint8_t UART3_GetChar(void);

#ifdef __cplusplus
}
#endif

#endif /* end of include guard: UART3_H */
