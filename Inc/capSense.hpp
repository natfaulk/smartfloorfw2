#ifndef CAP_SENSE_HPP
#define CAP_SENSE_HPP

#include "stm32f1xx_hal.h"

class CapSense
{
public:
  CapSense(GPIO_TypeDef* _sport, uint32_t _spin, GPIO_TypeDef* _rport, uint32_t _rpin, int32_t _id = -1);
  int32_t read(uint32_t nSamples);

  GPIO_TypeDef* sport;
  uint32_t spin;
  GPIO_TypeDef* rport;
  uint32_t rpin;
  int32_t id;
};

#endif
