#include "capsense.h"

// const int DEVICE_ID = 2;

unsigned int pin_map[25][2] = {{43,53}, {41,51}, {39,49}, {37,47}, {35,45},
                               {33,22}, {25,34}, {23,32}, {36,30}, {31,28},
                               {29,26}, {27,24}, {21,20}, {19,18}, {17,16},
                               {15,14}, {2,3},   {4,5},   {6,7},   {8,9},
                               {10,11}, {12,13}, {38,44}, {40,46}, {42,48}};

CapacitiveSensor **plates = new CapacitiveSensor*[25];

void setup()                    
{
//   plate10.set_CS_AutocaL_Millis(0xFFFFFFFF);     // turn off autocalibrate on channel 1 - just as an example
  Serial.begin(115200);

  for(int i=0;i<25;i++){
    plates[i] = new CapacitiveSensor(pin_map[i][0],pin_map[i][1]);
  }

  Serial.println("Setup Done"); 
}

int sampleVal = 10;

// long baselines[25] = {0};
// float smoothRate = 0.8;
long readings[25];
long avs[25];
float avfactor = 0.5;
bool avsinit = false;
// int counter = 0;

void loop()                    
{
  String data = "{\"ID\":\"__ID\"";
  for(int i=0;i<25;i++){
    readings[i] = plates[i]->capacitiveSensorRaw(sampleVal);
    if (!avsinit)
    {
      avs[i] = readings[i];
    } 
    else avs[i] = readings[i] * avfactor + avs[i] * (1 - avfactor);

    // if(counter > 10 && smoothRate != 0.1)
    //   smoothRate = 0.1;
      
    // baselines[i] = smoothRate * readings[i] + (1 - smoothRate) * baselines[i];
    
    data = data + ",\"p"+String(i+1)+"\":"+String(avs[i]);
    // Serial.print(readings[i] - baselines[i]);
    // if(i != 24)
    //   Serial.print(",");
  }


  // Serial.println();
  data = data +"}";
  
  Serial.println(data);

            
  // counter++;
  delay(10);                             // arbitrary delay to limit data to serial port 
}
