#include <cstring>

#include "catch.hpp"


#include "../esp_fw/constants.hpp"

TEST_CASE("Id substitution") {
  // verify the board ID is 4 chars
  REQUIRE(strlen(BOARD_ID) == 4);

  // verify that the strcpy offset is correct
  REQUIRE(strncmp(ID_SUBS_STRING + ID_OFFSET, "__ID", 4) == 0);
}
